package tw.com.twgame.common.net;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;


public class FtpUtil {
	 private FTPClient ftp;    
	 private static final String HOST = "dlftp01.idc.hinet.net";
	 private static final int PORT = 21;
	 private static final String username = "360twresourceupload";
	 private static final String password = "IorbJwJGP4NW";
//	 static
//	 {
//		 try {
//			connect("", "dlftp01.idc.hinet.net", 21, "360twresourceupload", "IorbJwJGP4NW");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}    
//	      
//	 }
	    /**  
	     *   
	     * @param path 上传到ftp服务器哪个路径下     
	     * @param addr 地址  
	     * @param port 端口号  
	     * @param username 用户名  
	     * @param password 密码  
	     * @return  
	     * @throws Exception  
	     */    
	    private  boolean connect(String path,String addr,int port,String username,String password) throws Exception {      
	        boolean result = false;      
	        ftp = new FTPClient();     
	        ftp.setControlEncoding("utf8");
	        int reply;      
	        ftp.connect(addr,port);      
	        ftp.login(username,password);      
	        ftp.setFileType(FTPClient.BINARY_FILE_TYPE);      
	        reply = ftp.getReplyCode();      
	        if (!FTPReply.isPositiveCompletion(reply)) {      
	            ftp.disconnect();      
	            return result;      
	        }    
	        ftp.changeWorkingDirectory("/"); 
            ftp.makeDirectory(path); 
            System.out.println("ftp directory: " + path); 
            ftp.changeWorkingDirectory(path); 

            System.out.println("ftp current directory: " + ftp.printWorkingDirectory()); 
	        //ftp.changeWorkingDirectory(path);      
	    
	        result = true;      
	        return result;      
	    } 
	   
	    /**  
	     *   
	     * @param file 上传的文件或文件夹  
	     * @throws Exception  
	     */    
	    private void transportFile(String folder,File file) throws Exception{
	    	connect(folder, HOST, PORT, username, password);
			
	        if(file.isDirectory()){           
	            ftp.makeDirectory(file.getName());                
	            ftp.changeWorkingDirectory(file.getName());      
	            String[] files = file.list();             
	            for (int i = 0; i < files.length; i++) {      
	                File file1 = new File(file.getPath()+"\\"+files[i] );      
	                if(file1.isDirectory()){      
	                	transportFile(folder,file1);      
	                    ftp.changeToParentDirectory();      
	                }else{                    
	                    File file2 = new File(file.getPath()+"\\"+files[i]);      
	                    FileInputStream input = new FileInputStream(file2);      
	                    ftp.storeFile(new String(file2.getName().getBytes("utf8"),"iso-8859-1"), input);      
	                    input.close();                            
	                }                 
	            }      
	        }else{      
	            File file2 = new File(file.getPath());      
	            FileInputStream input = new FileInputStream(file2);      
	            ftp.storeFile(file2.getName(), input);     
	         
	            input.close();        
	        }      
	        
	        ftp.logout();
	        ftp.disconnect();
	    }      
	    
	    
	    public static void upload(String ftpFolder,File f)
	    {
	    	UploadAsync uploadAsync = new UploadAsync(ftpFolder,f);
			Thread t = new Thread(uploadAsync);
			t.start();
	    }
	    
	    
	   static class UploadAsync implements Runnable {
			
			private File file = null;
			private String ftpFolder = "default";
			public UploadAsync(String ftpFolder,File file)
			{
				this.ftpFolder = ftpFolder;
				this.file = file;
			}
			public void run() {
				
				try {
					
					System.out.println("ftp upload start-->"+file.getPath());
					new FtpUtil().transportFile(ftpFolder,file);
					System.out.println("ftp upload end	-->"+file.getPath());
				
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			
			
		}
	  
}
