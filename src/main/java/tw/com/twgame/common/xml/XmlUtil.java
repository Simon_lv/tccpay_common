package tw.com.twgame.common.xml;

import java.io.IOException;
import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

public class XmlUtil 
{

	public static Element getRootElement(String xmlString) throws IOException, JDOMException
	{
		// 创建一个新的字符串
		StringReader read = new StringReader(xmlString);
		// 创建新的输入源SAX 解析器将使用 InputSource 对象来确定如何读取 XML 输入
		InputSource source = new InputSource(read);
		SAXBuilder sb = new SAXBuilder();
			
		Document doc=null;
			
		
		doc = sb.build(source);
		
		Element root = doc.getRootElement();
		return root;
	}
	
	
	public static void main(String[] args) throws IOException, JDOMException
	{
		String xmlString="<?xml version=\"1.0\" encoding=\"GBK\"?>"+
				"<files>"+
					"<file>"+
				"<state>1</state>"+
				"<type>apk</type>"+
				"<md5>66592da4f4bfb04a549c69f9c8df916e</md5>"+
				"<sign>安全/谨慎/危险/木马/不支持/审核中</sign>"+
				"<desc></desc>"+
				"<safe></safe>"+
				"<prop></prop>"+
				"</file>"+
				"</files>";
						
		Element root = getRootElement(xmlString);
	
		Element e = (Element)root.getChildren("file").get(0);
		
		for(Object obj:e.getChildren())
		{
			Element ele = (Element)obj;
			System.out.println("name-->"+ele.getName()+":"+"value-->"+ele.getText());
		}
	}
}
